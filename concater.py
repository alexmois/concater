from moviepy.editor import *
from time import gmtime, strftime, sleep
import os, shutil
import _thread
import time


def convert_video(name, path, done_dir):
    name += "_"
    dir_sep = os.sep
    extension = ".mp4"
    slices = []
    for root, dirs, files in os.walk(path):
        files.sort()
        for file in files:
            slices.append(VideoFileClip(root + dir_sep + file))
            print(name+"Added file #" + file)

    print(name+"Started concat "+strftime("%Y-%m-%d %H:%M:%S", gmtime())+"...")
    final_clip = concatenate_videoclips(slices)
    date = str(strftime("%Y-%m-%d_%H_%M_%S", gmtime()))
    filename = name+"_"+date+"_full_video"
    filename_tmp = filename + "_tmp"
    result_file_tmp = done_dir+dir_sep+filename_tmp+extension
    result_file = done_dir+dir_sep+filename+extension
    final_clip.write_videofile(filename=result_file_tmp, audio=False)
    print(name+"Done concat "+strftime("%Y-%m-%d %H:%M:%S", gmtime())+"...")
    shutil.rmtree(path)
    shutil.move(result_file_tmp, result_file)


def start():
    video_dir = os.path.dirname(os.path.abspath(__file__)) + os.sep + "video" + os.sep
    done_dir = os.path.dirname(os.path.abspath(__file__)) + os.sep + "done"
    computed_dir = os.path.dirname(os.path.abspath(__file__)) + os.sep + "computed" + os.sep

    if (os.path.isdir(computed_dir) == False):
        os.mkdir(computed_dir)

    if (os.path.isdir(done_dir) == False):
        os.mkdir(done_dir)

    print("####----------------####")
    print("#### VIDEO CONCATER ####")
    print("####----------------####")
    print("Current dir: " + video_dir)

    sub_dirs = []

    for dir in next(os.walk(video_dir))[1]:
        sub_dirs.append(video_dir + dir)
        convert_video(dir, video_dir + dir, done_dir)


start()